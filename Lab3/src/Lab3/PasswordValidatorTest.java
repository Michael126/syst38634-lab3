package Lab3;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testCheckPasswordLength() {
	assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("thisisavalidpassword"));
	
	
	
	
	}
	@Test
	public void testCheckPasswordLengthException() {
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("12345678"));

		}
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("password"));

		}
	@Test 
	public void testCheckPasswordLengthBoundaryOut() {
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("passwor") == false);

		}
	@Test
	public void testCheckPasswordDigits() {
	assertTrue("Password must have atleast 2 digits", PasswordValidator.checkPasswordDigits("thisismypassword123"));

	
	}
	@Test
	public void testCheckPasswordDigitsException() {
	assertTrue("Password must have atleast 2 digits", PasswordValidator.checkPasswordDigits("123"));

	
	}
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
	assertTrue("Password must have atleast 2 digits", PasswordValidator.checkPasswordDigits("password12"));

	
	}
	@Test
	public void testCheckPasswordDigitsBoundaryOut() {
	assertTrue("Password must have atleast 2 digits", PasswordValidator.checkPasswordDigits("password1") == false);
	
	assertTrue("Password must have atleast 2 digits", PasswordValidator.checkPasswordDigits("password") == false);

	
	}
}
