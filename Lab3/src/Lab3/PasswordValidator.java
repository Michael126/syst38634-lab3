package Lab3;

public class PasswordValidator {

	public static void main(String[] args) {

	}
	public static boolean checkPasswordLength (String password) {
		return password.length() >= 8; // Refactored code
	}
//		if(password.length() >= 8)
//		{
//			return true; 		 
//		}
//		else
//			return false;
//	}
	public static boolean checkPasswordDigits (String password) {
		int countDigits = 0;
		for(int i = 0; i < password.length(); i++)
		{
			if(password.charAt(i) > (char) 47 && password.charAt(i) < (char) 58) //if a character in password is from 0-9 count digit
			{
				
				countDigits++; //counts digits
				
			}
			
		}
		return countDigits >= 2;
//		if(countDigits >= 2) refactored
//		{
//			return true; 
//		}
//		else
//		{
//			return false;
//		}
		//System.out.println(countDigits); 
	}
}
